#!/bin/env python
import os
import subprocess
import shutil
import argparse
from datetime import datetime

def validate_user(u):
  if os.sep in u:  # make sure it won't make weird subfolders
    raise argparse.ArgumentTypeError("User must not contain path separators.")
  if not '@' in u or not '.' in u or ' ' in u:  # make sure it at least looks something like an email
    raise argparse.ArgumentTypeError("User must be an email address.")
  return u

parser = argparse.ArgumentParser()
parser.add_argument('--endpoint-dir', '-e', dest='endpoint_dir', type=str, required=True, help="Path to the base Globus directory.")
parser.add_argument('--endpoint-id', dest='endpoint_id', type=str, required=True, help="Globus endpoint ID.")
parser.add_argument('--user', '-u', dest='user', type=validate_user, required=True, help="Email for user who owns this run.")

parser.add_argument("--move", '-m', dest='move_files', type=str, default=[], nargs='*', help="Files to move into the appropriate Globus directory.")
parser.add_argument("--copy", '-c', dest='copy_files', type=str, default=[], nargs='*', help="Files to copy into the appropriate Globus directory.")

parser.add_argument('--sub-dir', '-s', dest='subdir', type=str,
                    default=os.environ.get('SLURM_JOB_ID', datetime.now().strftime("%b-%d-%Y %H:%M:%S")),
                    help="Prefix directory for Globus destination. Defaults to job ID.")

args = parser.parse_args()

# directory to store everything shared with this user
user_dir = os.path.join(args.endpoint_dir, args.user)  # directory for this user
if not os.path.exists(user_dir):
  # create it on the filesystem
  os.makedirs(user_dir)

  # give the user permission via email
  path = "{}:/{}/".format(args.endpoint_id, args.user)
  ret = subprocess.check_output(['globus', 'endpoint', 'permission', 'create', '--permissions', 'r', path, '--provision-identity', args.user, '-F', 'json'])
  #print ret
  #print "https://www.globus.org/app/transfer?origin_id={}&origin_path={}".format(args.endpoint_id, "%2F" + args.user + "%2F")

# final directory to put things in for this job
storage_dir = os.path.join(user_dir, args.subdir)
if not os.path.exists(storage_dir):
  os.makedirs(storage_dir)

def copyany(src, dest):
  # special "cp -R"-like behavior for directories
  if os.path.isdir(src):
    if os.path.exists(dest):
      dest = dest + '/' + os.path.basename(src)
    shutil.copytree(src, dest)
  else:
    shutil.copy(src, dest)  # cp-like for files

# finally, copy/move files to storage_dir
files = [(name, shutil.move) for name in args.move_files] + [(name, copyany) for name in args.copy_files]
for path, operation in files:
  src = os.path.realpath(path)
  dest = storage_dir
  operation(src, dest)
  print args.endpoint_id + ":/" + os.path.relpath(storage_dir, args.endpoint_dir) + '/' + os.path.relpath(src, os.getcwd())
