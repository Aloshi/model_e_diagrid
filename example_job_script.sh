#!/bin/bash
#SBATCH --job-name="model_e"
#SBATCH --output="OUT.LOG"
#SBATCH -p shared
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
#SBATCH --export=ALL
#SBATCH -t 00:30:00

# Comet's Python is 2.6 by default, need the module for 2.7
module load python

# need an MPI implementation loaded
module swap intel intel/2016.3.210
module load mvapich2_ib

# run model_e to generate the data
ibrun $HOME/model_e_code/model_e/bin/model_e_contour_step

# generate visualizations
~/model_e_scripts/model_e_viz.py -o visualization --render-iso

# move/copy data files to globus storage area for the given user, save file list in globus_files.txt
GLOBUS_ENDPOINT_ID=35cc1486-8dc4-11e7-a9f9-22000a92523b
GLOBUS_ENDPOINT_DIR=/oasis/projects/nsf/ios111/lofquist/shared/hpcr
GLOBUS_EMAIL=lofquist@iastate.edu
~/model_e_scripts/model_e_globus.py --endpoint-id "$GLOBUS_ENDPOINT_ID" --endpoint-dir "$GLOBUS_ENDPOINT_DIR" --user "$GLOBUS_EMAIL" \
    --move data_final.plt data_init.plt snapshot_*.plt --copy log.csv config.txt OUT.LOG > globus_files.txt
