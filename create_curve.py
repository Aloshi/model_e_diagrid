#!/usr/bin/env python
import csv
import os
import argparse

"""create_curve.py: Transforms input CSV file to output data into VisIt's curve file format
Several output files are created
1. Cumulative curve files for each time step as progress####.curve
2. Last curve file is sym linked as "all.curve" which has all timesteps

@see Curve file format : http://www.visitusers.org/index.php?title=Reading_curve_data

__author__ = "Amit Chourasia"
__copyright__ = "Copyright (C) Regents of California"
__license__ = "Public Domain"
__status__ = "Prototype"
__version__ = "0.1"
__date__ = "Mar 1, 2017"
"""

# Change these input parameters
#############################################
parser = argparse.ArgumentParser()
parser.add_argument('--log-file', dest='logfile', type=str, default='log.csv')
parser.add_argument('--frequency', type=int, default=100, help="Frequency to generate curves from log (e.g. 1 would be every line in the log, 2 would be every other, etc.).")
parser.add_argument('--output-dir', '-o', dest='output_dir', type=str, default='curves')
parser.add_argument('--output-prefix', '-p', dest='output_filename_prefix', type=str, default='energy_ts_')
parser.add_argument('--last-file-symlink', dest='last_file_symlink', type=str, default='energy_all.curve')

args = parser.parse_args()

#logfile = 'spl/snap_log.csv'
#output_dir = 'spl/curves'
#output_filename_prefix = 'progress'
#last_file_symlink = 'all.curve'
#############################################

if not os.path.exists(args.output_dir):
  os.makedirs(args.output_dir)

# Read existing data from csv file into a dict
result = {} # placeholder dict
try:
    with open(args.logfile) as csvfile:
      reader = csv.DictReader(csvfile, delimiter=' ')
      for row in reader:
        for column, value in row.iteritems():
          result.setdefault(column, []).append(value)
except IOError:
    print 'ERROR: file not found:', args.logfile
    exit(1)

# Write curve files with cumulative data for each step
# first curve file will contain one row
# second curve file will contain two rows (i.e. first and second)
# so on ..
l = len (result['iteration'])
for i in xrange(1, l+1):
    # respect frequency
    if i % args.frequency != 0:
        continue

    # data segments
    edata = [['#', 'energy']]
    pdata = [['#', 'partition_func']]
    rdata = [['#', 'residual']]
    tdata = [['#', 'time']]

    for j in xrange(0, i):
        edata.append([ result['iteration'][j] , float(result['energy'][j]) ])
        pdata.append([ result['iteration'][j] , float(result['partition_func'][j]) ])
        rdata.append([ result['iteration'][j] , float(result['residual'][j]) ])
        tdata.append([ result['iteration'][j] , float(result['time'][j]) ])

    outfile = args.output_dir + '/' + args.output_filename_prefix + str(i).zfill(4) + '.curve'
    print 'writing', outfile
    try:
        with open(outfile, 'w') as f:
            a = csv.writer(f, delimiter=' ', quoting = csv.QUOTE_NONE, lineterminator="\n")
            a.writerows(edata)
            #a.writerows(' ') # empty line
            a.writerows(pdata)
            #a.writerows(' ') # empty line
            a.writerows(rdata)
            #a.writerows(' ') # empty line
            a.writerows(tdata)

        # Add a symlink to last file as 'all.curve'
        if i == l:
            sym = args.output_dir + '/' + args.last_file_symlink
            try:
                os.remove(sym)
            except OSError:
                pass

            print 'symlinking last file', outfile, 'as', sym
            os.symlink( os.path.relpath(outfile, args.output_dir), sym)
    except IOError:
        print 'ERROR: Cannot write to file:', outfile
        exit(1)

