import os
import sys
import re
import glob
from time import sleep

# Change the following
####################################################################
# Data we wish to render
#datafile = ('/home1/00521/tg457718/morphology/data/spl/snap*.plt')
#myOutputDir = '/home1/00521/tg457718/morphology/renders/spl' # provide absolute path
#myOutFilename = 'snap'
#width = 512
#height = 512
#t = 0 # start step
####################################################################

import argparse
parser = argparse.ArgumentParser(description='Fetch arguments to python script')
parser.add_argument('-myp', '-myparallel', # caution -p is used by visit
                     dest='parallel',
                     action='store_true',
                     help="Turn on parallel execution")
parser.add_argument('--datafiles', '-d', dest='datafile', type=str, default='snapshot_*.plt')
parser.add_argument('--output-dir', '-o', dest='myOutputDir', type=str, default='iso')
parser.add_argument('--output-name', dest='myOutFilename', type=str, default='iso_ts')
parser.add_argument('--width', dest='width', type=int, default=512)
parser.add_argument('--height', dest='height', type=int, default=512)
parser.add_argument('--timestep', '-t', dest='timestep', type=int, default=0, help='Start timestep.')

# parse known arguments, ignore the rest
args, unknown = parser.parse_known_args()
if args.parallel:
    print 'Setting parallel execution for visit'
    import os
    nodes = os.environ['SLURM_JOB_NUM_NODES']
    cores = os.environ['SLURM_TACC_CORES']
    tasks = os.environ['SLURM_NTASKS']
    home = os.environ['HOME']
    print 'TACC ENV, Nodes: ', nodes , 'Cores: ', cores
    totalCores =  int(cores) # slurm provides cores = nodes *cores
    #OpenComputeEngine("localhost", ("-nn", "1", "-np", str( totalCores ), "-l", "ibrun") )
    OpenComputeEngine("localhost", ("-nn", "1", "-np", str( totalCores ), "-par", "-l", "ibrun") )

if not os.path.exists(args.myOutputDir):
  os.makedirs(args.myOutputDir)

# Here we will setup the visualization programmatically, although its possible to use pre created session files
# Open database
db = 'localhost:' + args.datafile + ' database'
OpenDatabase(db,args.timestep)
n = TimeSliderGetNStates() # find number of time steps


# Add pseudocolor plot
AddPlot("Pseudocolor", "density_0", 1, 1)
PseudocolorAtts = PseudocolorAttributes()
PseudocolorAtts.minFlag = 1
PseudocolorAtts.min = 0
PseudocolorAtts.maxFlag = 1
PseudocolorAtts.max = 1
PseudocolorAtts.colorTableName = "RdBu"
PseudocolorAtts.invertColorTable = 0
SetPlotOptions(PseudocolorAtts)


# Add contour plot
AddPlot("Contour", "density_0", 1, 1)
ContourAtts = ContourAttributes()
ContourAtts.defaultPalette.categoryName = "Standard"
ContourAtts.changedColors = (0)
ContourAtts.colorType = ContourAtts.ColorByMultipleColors  # ColorBySingleColor, ColorByMultipleColors, ColorByColorTable
ContourAtts.colorTableName = "Default"
ContourAtts.invertColorTable = 0
ContourAtts.legendFlag = 1
ContourAtts.lineStyle = ContourAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
ContourAtts.lineWidth = 0
ContourAtts.SetMultiColor(0, (0, 0, 0, 255))
ContourAtts.SetMultiColor(1, (0, 255, 0, 255))
ContourAtts.SetMultiColor(2, (0, 0, 255, 255))
ContourAtts.SetMultiColor(3, (0, 255, 255, 255))
ContourAtts.SetMultiColor(4, (255, 0, 255, 255))
ContourAtts.SetMultiColor(5, (255, 255, 0, 255))
ContourAtts.SetMultiColor(6, (255, 135, 0, 255))
ContourAtts.SetMultiColor(7, (255, 0, 135, 255))
ContourAtts.SetMultiColor(8, (168, 168, 168, 255))
ContourAtts.SetMultiColor(9, (255, 68, 68, 255))
ContourAtts.contourValue = (0.3)
ContourAtts.contourMethod = ContourAtts.Value  # Level, Value, Percent
SetPlotOptions(ContourAtts)

# try and sync output filename suffixes with input filename suffixes
iters = []
for i, name in enumerate(sorted(glob.glob(args.datafile))):
    match = re.search(r'snapshot_(\d+)', name)
    if match:
        iters.append(int(match.group(1)))
    else:
        iters.append(i)
assert len(iters) == n

# Animate, render and save
for i in range(args.timestep, n):
    # Save output settings
    s = SaveWindowAttributes() # specify window attributes
    s.format = s.PNG # for saving to a file
    s.width, s.height = args.width, args.height #
    s.outputToCurrentDirectory = 0 # do not write to current dir
    s.outputDirectory = args.myOutputDir # write images to this location
    s.fileName = args.myOutFilename + "_%04d" % (iters[i]) # output image filename
    s.family = 0 # disable appending 0000 0001 to filename
    SetSaveWindowAttributes(s) #

    # Increment time slider
    SetTimeSliderState(i)
    DrawPlots()
    SaveWindow() # save


# Clean up
DeleteAllPlots() #
CloseDatabase(db) #
ClearCacheForAllEngines() #
CloseComputeEngine() #
sleep(10) # wait for engines to terminate
sys.exit() # required by VisIt
