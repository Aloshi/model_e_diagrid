This repository contains support scripts for the "polyrun" DiaGrid tool.

* `create_curve.py` and `render-curve.py` generate and render an energy plot from the Model E "log.csv" file. `render-curve.py` is a Visit script (so don't try to run it under normal Python).
* `render-iso.py` is another Visit script for rendering the morphology at each timestep. It reads in Model E's "snapshot_*.plt" files and saves them as PNG files.
* `model_e_viz.py` ties together create_curve.py/render-curve.py/render-iso.py. `./model_e_viz.py -o visualization --create-curves --render-curves --render-iso` will run the previous scripts and save the results in `visualization/energy_plot/energy_ts_*.png` and `visualization/iso/iso_ts_*.png`.
* `model_e_globus.py` moves files to the Globus storage area for a given user (and creates it with appropriate permissions if it doesn't exist). It requires the Globus CLI to be available as `globus` in the shell. It moves/copies files to `[globus_endpoint_dir]/[user]/[SLURM_JOB_ID]` and prints out the Globus paths of the files. The Globus endpoint and user email must be given as arguments.

See [example_job_script.sh](example_job_script.sh) for usage.
