import os
import sys
from time import sleep

# Change the following
####################################################################
# Data we wish to render
#curvefiles = ('/home1/00521/tg457718/morphology/data/spl/curves/progress*.curve')
#lastfile = ('/home1/00521/tg457718/morphology/data/spl/curves/all.curve')
#myOutputDir = '/home1/00521/tg457718/morphology/renders/spl' # provide absolute path
#myOutFilename = 'progress'
#width = 512
#height = 512
#timestep = 0 # start step
####################################################################


import argparse
parser = argparse.ArgumentParser(description='Fetch arguments to python script')
parser.add_argument('-myp', '-myparallel', # caution -p is used by visit
                     dest='parallel',
                     action='store_true',
                     help="Turn on parallel execution")
parser.add_argument('--curvelist', '-c', dest='curvefiles', type=str, default='curves/energy_ts_*.curve')
parser.add_argument('--last-file', dest='lastfile', type=str, default='energy_all.curve')
parser.add_argument('--output-dir', '-o', dest='myOutputDir', type=str, default='energy_plots')
parser.add_argument('--output-name', dest='myOutFilename', type=str, default='energy_ts')
parser.add_argument('--width', dest='width', type=int, default=512)
parser.add_argument('--height', dest='height', type=int, default=512)
parser.add_argument('--timestep', '-t', dest='timestep', type=int, default=0, help='Start timestep.')

# parse known arguments, ignore the rest
args, unknown = parser.parse_known_args()
if args.parallel:
    import os
    nodes = os.environ['SLURM_JOB_NUM_NODES']
    cores = os.environ['SLURM_TACC_CORES']
    tasks = os.environ['SLURM_NTASKS']
    home = os.environ['HOME']
    print 'TACC ENV, Nodes: ', nodes , 'Cores: ', cores
    totalCores =  int(cores) # slurm provides cores = nodes *cores
    #OpenComputeEngine("localhost", ("-nn", "1", "-np", str( totalCores ), "-l", "ibrun") )
    OpenComputeEngine("localhost", ("-nn", "1", "-np", str( totalCores ), "-par", "-l", "ibrun") )

if not os.path.exists(args.myOutputDir):
  os.makedirs(args.myOutputDir)

# Here we will setup the visualization programmatically, although its possible to use pre created session files
# Open curve files database
dbseries = 'localhost:' + args.curvefiles + ' database'
OpenDatabase(dbseries, args.timestep)
n = TimeSliderGetNStates() # find number of time steps
print n, 'TIME STEPS'

# Add curve plots
AddPlot("Curve", "residual")
CurveAtts = CurveAttributes()
CurveAtts.showLines = 1 
CurveAtts.lineStyle = CurveAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
CurveAtts.lineWidth = 0
CurveAtts.curveColorSource = CurveAtts.Custom  # Cycle, Custom
CurveAtts.curveColor = (255, 0, 0, 255)
CurveAtts.showLegend = 1
CurveAtts.showLabels = 0
CurveAtts.designator = ""
SetActivePlots((0))
SetPlotOptions(CurveAtts)

AddPlot("Curve", "energy")
CurveAtts = CurveAttributes()
CurveAtts.showLines = 1
CurveAtts.lineStyle = CurveAtts.SOLID  # SOLID, DASH, DOT, DOTDASH
CurveAtts.lineWidth = 0
CurveAtts.curveColorSource = CurveAtts.Custom  # Cycle, Custom
CurveAtts.curveColor = (0, 255, 0, 255)
CurveAtts.showLegend = 1
CurveAtts.showLabels = 0
CurveAtts.designator = ""
SetActivePlots((1))
SetPlotOptions(CurveAtts)



# Open final curve file
dblast = 'localhost:' + args.lastfile 
OpenDatabase(dblast)


# Add curve  plot
AddPlot("Curve", "residual")
CurveAtts = CurveAttributes()
CurveAtts.showLines = 0 
CurveAtts.showPoints = 1
CurveAtts.symbol = CurveAtts.Point  # Point, TriangleUp, TriangleDown, Square, Circle, Plus, X
CurveAtts.pointSize = 5
CurveAtts.pointFillMode = CurveAtts.Static  # Static, Dynamic
CurveAtts.pointStride = 1
CurveAtts.symbolDensity = 50
CurveAtts.curveColorSource = CurveAtts.Custom  # Cycle, Custom
CurveAtts.curveColor = (255, 60, 60, 255)
CurveAtts.showLegend = 1
CurveAtts.showLabels = 0
CurveAtts.designator = ""
SetActivePlots((2))
SetPlotOptions(CurveAtts)

AddPlot("Curve", "energy")
CurveAtts = CurveAttributes()
CurveAtts.showLines = 0
CurveAtts.showPoints = 1
CurveAtts.symbol = CurveAtts.Point  # Point, TriangleUp, TriangleDown, Square, Circle, Plus, X
CurveAtts.pointSize = 5
CurveAtts.pointFillMode = CurveAtts.Static  # Static, Dynamic
CurveAtts.pointStride = 1
CurveAtts.symbolDensity = 50
CurveAtts.curveColorSource = CurveAtts.Custom  # Cycle, Custom
CurveAtts.curveColor = (60, 255, 60, 255)
CurveAtts.showLegend = 1
CurveAtts.showLabels = 0
CurveAtts.designator = ""
SetActivePlots((3))
SetPlotOptions(CurveAtts)


ActivateDatabase(dbseries)
# Animate, render and save
for i in range(args.timestep, n):
    # Save output settings
    s = SaveWindowAttributes() # specify window attributes
    s.format = s.PNG # for saving to a file
    s.width, s.height = args.width, args.height #
    s.outputToCurrentDirectory = 0 # do not write to current dir
    s.outputDirectory = args.myOutputDir # write images to this location
    s.fileName = args.myOutFilename + "_%04d" % (i) # output image filename
    s.family = 0 # disable appending 0000 0001 to filename
    SetSaveWindowAttributes(s) #

    # Increment time slider
    SetTimeSliderState(i)
    DrawPlots()
    SaveWindow() # save


# Clean up
DeleteAllPlots() #
CloseDatabase(dbseries) #
CloseDatabase(dblast) #
ClearCacheForAllEngines() #
CloseComputeEngine() #
sleep(10) # wait for engines to terminate
sys.exit() # required by VisIt

