#!/bin/env python

import os
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--data-dir", '-d', dest='data_dir', type=str, default=os.getcwd(), help="Directory containing result data.")
parser.add_argument("--scripts-dir", dest='scripts_dir', type=str, default=os.path.dirname(__file__), help="Directory containing the Visit Python scripts.")
parser.add_argument("--output-dir", '-o', dest='output_dir', type=str, default=os.getcwd(), help="Directory to write output files to.")
parser.add_argument("--create-curves", dest='create_curve', default=False, action='store_true')
parser.add_argument("--render-curves", dest='render_curve', default=False, action='store_true')
parser.add_argument("--render-iso", dest='render_iso', default=False, action='store_true')

args = parser.parse_args()

if not os.path.exists(args.output_dir):
  os.makedirs(args.output_dir)

args.output_dir = os.path.realpath(args.output_dir)

if args.create_curve:
  script_path = args.scripts_dir + "/create_curve.py"
  logfile = args.data_dir + "/log.csv"
  subprocess.check_call("\"" + script_path + "\" --log-file \"" + logfile + "\"", shell=True, cwd=args.output_dir)

if args.render_curve:
  script_path = args.scripts_dir + "/render-curve.py"
  curvelist = args.output_dir + "/curves/energy_ts_*.curve"
  lastfile = args.output_dir + "/curves/energy_all.curve"
  subprocess.check_call("module load visit; export DISPLAY=:0.0; visit -cli -nowin -s \"" + script_path + "\" --curvelist \"" + curvelist + "\" --last-file \"" + lastfile + "\"", shell=True, cwd=args.output_dir)

if args.render_iso:
  script_path = args.scripts_dir + "/render-iso.py"
  datafiles = args.data_dir + "/snapshot_*.plt"
  subprocess.check_call("module load visit; export DISPLAY=:0.0; visit -cli -nowin -s \"" + script_path + "\" --datafiles \"" + datafiles + "\"", shell=True, cwd=args.output_dir)
